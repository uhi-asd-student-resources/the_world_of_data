USE templates;

-- we need to turn off foreign key checks as we cannot truncate tables that
-- are involved with foreign key checks.
SET FOREIGN_KEY_CHECKS=0;

-- delete all records AND reset auto_increment counters to zero
-- alternative DELETE * FROM users; will delete records but NOT reset counters.
TRUNCATE TABLE users;

-- turn foreign key checks back on
SET FOREIGN_KEY_CHECKS=1;

-- an auto_increment field can also be left out
INSERT INTO users (display_name, login_email, secret_password, secret_salt, degree ) VALUES ( 
    'Edward',
    'edward@example.com',
    SHA1(CONCAT('dogf1sh','ag56h25h345h12343')),
    'ag56h25h345h12343',
    'Fishing by JR Hartley'
);

INSERT INTO users (display_name, login_email, secret_password, secret_salt, degree ) VALUES ( 
    'Alice',
    'alice@example.com',
    SHA1(CONCAT('dogf2sh','ag56h25h345h12344')),
    'ag56h25h345h12344',
    'Fishing by JR Hartley'
);

INSERT INTO users (display_name, login_email, secret_password, secret_salt, degree ) VALUES ( 
    'Bob',
    'bob@example.com',
    SHA1(CONCAT('dogf3sh','ag56h25h345h12345')),
    'ag56h25h345h12345',
    'Fishing by JR Hartley'
);

INSERT INTO users (display_name, login_email, secret_password, secret_salt, degree ) VALUES ( 
    'Charlie',
    'charlie@example.com',
    SHA1(CONCAT('dogf4sh','ag56h25h345h12345')),
    'ag56h25h345h12345',
    'Fishing by JR Hartley'
);


-- we also add multiple rows in the same query
INSERT INTO templates ( template_id, fk_user_id, name ) VALUES   (1, 1, "template1"), 
                                                    (2, 2, "template2"), 
                                                    (3, 3, "template3");

INSERT INTO sections ( fk_template_id, name, fk_section_type_id ) VALUES 
    (1, "description", 1),
    (2, "description", 1),
    (3, "description", 1),
    (1, "image", 3);


