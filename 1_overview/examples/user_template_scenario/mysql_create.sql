-- Templates database for knowledge base
-- Author: Me
-- Date: 25/11/2020
-- Keep this file in version control.
CREATE DATABASE IF NOT EXISTS templates;

USE templates;

-- here is a good discussion of when to store the salt (and pepper)
-- https://security.stackexchange.com/questions/17421/how-to-store-salt
CREATE TABLE IF NOT EXISTS users (
    user_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    display_name VARCHAR(100),
    login_email VARCHAR(100),
    secret_password VARCHAR(64),            -- this should be stored as a hash(password+salt)
    secret_salt VARCHAR(64),                -- this can be plain text
    degree VARCHAR(100) DEFAULT NULL        -- optional value
);

CREATE TABLE IF NOT EXISTS templates (
    template_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    fk_user_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (fk_user_id)
        REFERENCES users (user_id)
        ON DELETE CASCADE               -- delete this record when user is deleted
);

CREATE TABLE IF NOT EXISTS section_types (
    section_type_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS sections (
    section_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    fk_template_id INT UNSIGNED NOT NULL,
    fk_section_type_id SMALLINT UNSIGNED NOT NULL,
    FOREIGN KEY (fk_template_id)
        REFERENCES templates (template_id) 
        ON DELETE CASCADE,               -- delete this record when template is deleted
    FOREIGN KEY (fk_section_type_id)
        REFERENCES section_types (section_type_id)
);
