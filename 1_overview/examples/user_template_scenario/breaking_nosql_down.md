# Breaking the NoSQL down into

```
{                                               # denotes an object - potential table?
    "id": 1,                                    # unique identifier we can use
    "_rev": 10,                                 # this is to do with CAP, we can ignore
    "name": "John Smith",                       # our first field
    "templates": [                              # transform into a table of "templates"
        {                                       # first "record"    
            "name": "reflection1",              # name is a field
            "sections": [                       # transform sections into a table with a foreign key to 'templates'
                {
                    "name": "description",      # a section has a name and a type
                    "type": "text"              # type is a limited set of values, either ENUM or lookup table (preferred).
                },
                {
                    "name": "start",            
                    "type": "list"
                },
                {
                    "name": "stop",
                    "type": "list"
                },
                {
                    "name": "continue",
                    "type": "list"
                }
            ]
        },
        {
            "name": "reflection2",
            "sections": []                      # empty sections handles by having no records in 'sections' table for this template
        },
        {
            "name": "reflection3"               # no sections field handled by having no records in 'sections' table for this template
        }
    ]
}
```

## What are our entities and fields?

* Users
  * user_id
  * name
* Templates
  * template_id         - unique number for each record in our table
  * fk_users_id         - points to a record in the Users table
  * name
* Sections
  * section_id          - unique number for each record in our table
  * fk_templates_id     - points to a record in the Templates table
  * name
  * order               - which order the sections appear
  * fk_type_id          - each record will have a pointer to the types table
* Types 
  * type_id             - unique number for each record in our table
  * name

## What if we had a many-to-many relationship?

If we wanted sections to be used in multiple templates we would introduce the following:

* lnk_templates_sections
  * lnk_template_section_id     - unique id for each record in table
  * fk_templates_id
  * fk_section_id

No change to the templates table:

* templates             
  * template_id
  * fk_users_id
  * name

Remove the foreign key to the templates table from sections.

* sections              
  * section_id
  * name
  * fk_type_id

