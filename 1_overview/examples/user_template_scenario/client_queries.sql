USE templates;

-- The * selects all fields in the tables.
-- If there are conflicts then you cannot use * in many SQL versions
-- hence we pick unique names for fields like 'id'.
SELECT * FROM users;

SELECT * FROM templates;

-- LEFT JOIN is a VERY COMMON query and is efficient for 1-N joins.
-- It joins 1 from users with matching values in templates.
--
-- Syntax: FROM left LEFT JOIN right ON left.field=right.field
--
-- It will show ALL records in the left table even if no matches are found in the right table.
SELECT user_id, template_id FROM users LEFT JOIN templates ON users.user_id=templates.fk_user_id;
SELECT user_id, template_id FROM users a LEFT JOIN templates b ON a.user_id=b.fk_user_id;
SELECT user_id, template_id FROM users AS a LEFT JOIN templates AS b ON a.user_id=b.fk_user_id;

-- lets pick for a particular user
SELECT user_id, template_id 
FROM users AS a LEFT JOIN templates AS b ON a.user_id=b.fk_user_id
WHERE user_id=1

-- error as this will return a display_name which has not relevance to the COUNT.
-- this is a hard error in some databases.
SELECT display_name, COUNT(template_id) FROM users u LEFT JOIN templates t ON u.user_id=t.fk_user_id 

-- groups properly
SELECT display_name, COUNT(template_id) 
FROM users u LEFT JOIN templates t ON u.user_id=t.fk_user_id 
GROUP BY display_name;

-- lets select based on our grouping
SELECT display_name, COUNT(template_id) 
FROM users u LEFT JOIN templates t ON u.user_id=t.fk_user_id 
WHERE user_id > 2
GROUP BY display_name
HAVING COUNT(template_id) > 2;

-- sort
-- you can sort by multiple fields as well.
SELECT display_name, COUNT(template_id) 
FROM users u LEFT JOIN templates t ON u.user_id=t.fk_user_id 
WHERE user_id > 2
GROUP BY display_name
HAVING COUNT(template_id) > 2;
ORDER BY display_name ASC

SELECT display_name, COUNT(template_id) 
FROM users u LEFT JOIN templates t ON u.user_id=t.fk_user_id 
WHERE user_id > 2
GROUP BY display_name
HAVING COUNT(template_id) > 2;
ORDER BY display_name DESC

-- paging uses LIMIT M OFFSET N
-- print first 2
SELECT * FROM templates LIMIT 2 OFFSET 0    
-- print second 2
SELECT * FROM templates LIMIT 2 OFFSET 2


