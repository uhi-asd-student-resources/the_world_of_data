USE templates;

-- note we can put a 0 in the auto_increment field and it will
-- be assigned the next number available
INSERT INTO section_types ( section_type_id, name ) VALUES ( 0, "text" );
INSERT INTO section_types ( section_type_id, name ) VALUES ( 0, "list" );
INSERT INTO section_types ( name ) VALUES ( "image" );