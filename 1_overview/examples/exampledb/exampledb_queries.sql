-- INNER JOIN (cartesian product) 72 rows
-- Note we are not specifying any keys to match on here
-- 1 row in left_table, matched with N rows from right_table
-- Many people like these BUT it will not return the situation where the LEFT_TABLE has 
-- no counterpart in the RIGHT_TABLE, which you almost always want to know.
select * from left_table, right_table;
select * from left_table JOIN right_table;
select * from left_table INNER JOIN right_table;
select * from left_table INNER JOIN right_table WHERE left_id=right_id;

-- we can rows with left_table.name != NULL using the IS NOT
select * from left_table, right_table WHERE left_table.name IS NOT NULL;
select * from left_table, right_table WHERE left_table.name IS NULL;

-- CROSS JOIN 3 rows
select * from left_table l CROSS JOIN right_table r ON l.name=r.name;
-- a natural join will match all fields that have the same name in both tables.
select * from left_table NATURAL JOIN right_table;

-- LEFT JOIN 10 rows (includes NULL)
-- LEFT JOINS are the most common join you will ever need.
select * from left_table l LEFT JOIN right_table r ON l.name=r.name;
select * from left_table l LEFT JOIN right_table r ON r.name=l.name;

-- RIGHT JOIN 8 rows (includes NULL)
select * from left_table l RIGHT JOIN right_table r ON l.name=r.name;
select * from left_table l RIGHT JOIN right_table r ON r.name=l.name;

-- SELF JOIN is where we join the table to itself
-- can you think of a situation where this might be useful?
SELECT * FROM left_table AS A LEFT JOIN left_table AS B ON A.left_id=(B.left_id+1)

-- you can think of a join as laying tables side by side
-- to lay them one under the other you use UNION like this:
(select * from left_table)  UNION ALL (select * from left_table);
-- by default UNION by itself will only show DISTINCT rows like this
(select * from left_table)  UNION (select * from left_table);
