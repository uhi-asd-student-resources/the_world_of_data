# Example Database

This is a simple 2 table database for exploring joins.

## Instructions

```
mysql -u root -p < mysql_exampledb_create.sql
mysql -u root -p < exampledb_data.sql
```

You can then cut paste queries int the mysql client.
