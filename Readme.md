# World of data

A set of learning resources on databases.

## Chapter 1

This is based on a scenario that came up in a student project.  There are resources for investigating JOINS in MySQL/MariaDB.

## Installation

Follow the instructions for the vendor either MySQL or MariaDB.
You then copy and paste from the .sql files as required.

In linux you can do the following to use the sql files directly:

```
mysql -u root -p < my_database_file.sql
```

## Security

* Use prepared statements, not string concatenation to create queries for our database.
* Check all incoming variables and to not use variables directly e.g. using $_GET['myvar'] without first assigning it to a variables and checking for type and contents.
* Watch out in text input for html and script tags.
* Lock down the database to just what the webapp requires.
* If the language offers a specific "sanitise" set of functions, use them - don't try and reinvent these as they are easy to get wrong.
* Ensure you sanitise any paging arguments because they are easy to draw out full copies of a table.

## Tools

* Draw.io for drawing Entity Relationship Diagrams (ERD)
* MySQL Workbench for generating ERDs from an existing database
* phpMyAdmin for a web client for interacting with a database
