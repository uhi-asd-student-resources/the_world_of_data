# Cypher Optimisation

## Parameters

```
:param year => 2000

:params {year: 2000, name: "Keith Richards"}
```

## EXPLAIN

You focus on how many rows are being analysed and if your MATCH criteria are the most efficient.  It can also help when you believe there should be rows but nothing is being returned, there may be logical bugs in your WHERE clauses.

A major goal for a good graph data model and query is one where the number of rows processed is minimized. You want to see the use of indexes in your queries.

```
EXPLAIN MATCH (r:Person)-[rel:REVIEWED]->(m:Movie)<-[:ACTED_IN]-(a:Person)
WHERE m.released = $year AND
      rel.rating > $ratingValue
RETURN  DISTINCT r.name, m.title, m.released, rel.rating, collect(a.name)
```

## PROFILE

Find out what part of the query is taking a long time, maybe add an index or rework design.

Here you can view the cache hits and most importantly the number of times the graph engine accessed the database (db hits). This is an important metric that will affect the performance of the Cypher statement at run-time. 

```
PROFILE MATCH (r:Person)-[rel:REVIEWED]->(m:Movie)<-[:ACTED_IN]-(a:Person)
WHERE m.released = $year AND
      rel.rating > $ratingValue
RETURN  DISTINCT r.name, m.title, m.released, rel.rating, collect(a.name)
```

## AVOID

Avoid queries like:

```
// completes, but generates a lot of output
MATCH (a)--(b)--(c)--(d)--(e)--(f)--(g) RETURN a

// take a long time to execute
MATCH (a), (b), (c), (d), (e) RETURN count(id(a))
```

