# Cypher CRUD Cheat Sheet



## Nodes

Create will create the node and if it exists already it will create a duplicate.

Merge logic is:

```
IF node DOES NOT EXIST THEN 
    create node
    ON CREATE SET do something
ELSE 
    ON MATCH SET do something
END IF

```

Examples:

```
CREATE (:Movie {title: 'Forrest Gump'})

CREATE
(:Person {name: 'Michael Caine', born: 1933}),
(:Person {name: 'Liam Neeson', born: 1952}),
(:Person {name: 'Katie Holmes', born: 1978}),
(:Person {name: 'Benjamin Melniker', born: 1913})


// to stop new nodes with same properties use MERGE
MERGE
(:Person {name: 'Michael Caine', born: 1933}),
(:Person {name: 'Liam Neeson', born: 1952}),
(:Person {name: 'Katie Holmes', born: 1978}),
(:Person {name: 'Benjamin Melniker', born: 1913})

MERGE (m:Movie {title: 'Forrest Gump'})
ON CREATE SET m.released = 1994
RETURN m

MERGE (m:Movie {title: 'Forrest Gump'})
ON CREATE SET m.released = 1994
ON MATCH SET m.tagline = "Life is like a box of chocolates...you never know what you're gonna get."
RETURN m

MERGE (m:Movie {title: 'Forrest Gump'})
ON MATCH SET m:OlderMovie
RETURN labels(m)
```

Add a label to each of a set of nodes

```
MATCH (m:Movie)
WHERE m.released < 2010
SET m:OlderMovie
RETURN DISTINCT labels(m)

MATCH (p:Person)
WHERE p.name STARTS WITH 'Robin'
SET p:Female
```

Modify the label and properties of a set of nodes (in this case 1 node):

```
MATCH (m:Movie)
WHERE m.title = 'Forrest Gump'
SET m:OlderMovie,
    m.released = 1994,
    m.tagline = "Life is like a box of chocolates...you never know what you're gonna get.",
    m.lengthInMinutes = 142


MATCH (m:Movie)
WHERE m.title = 'Forrest Gump'
SET m:OlderMovie,
    m += {released: 1994,
    tagline: "Life is like a box of chocolates...you never know what you're gonna get.",
    lengthInMinutes: 142
    }
```

Remove a label from each of a set of nodes

```
MATCH (p:Female)
REMOVE p:Female
```

Remove property from node:

```
// either set property to null
MATCH (m:Movie) 
WHERE m.title="Forrest Gump" 
SET m.lengthInMinutes = null

// or use
MATCH (m:Movie) 
WHERE m.title="Forrest Gump" 
REMOVE m.lengthInMinutes
```

Delete nodes can only happen once all relationships have been removed.

```
// delete node along with all relationships
MATCH (m:Movie)
WHERE m.title = 'Forrest Gump'
DETACH DELETE m
```



## Relationships

Create relationships, a direction of the relationship MUST be specified.  Bidirectional relationships are modelled explicitly as 2 relationships, one between nodes A and B, and the other between B and A.

```
MATCH (m:Movie)
WHERE m.title = 'Forrest Gump'
MATCH (p:Person)
WHERE p.name = 'Tom Hanks' OR p.name = 'Robin Wright' OR p.name = 'Gary Sinise'
CREATE (p)-[:ACTED_IN]->(m)

// create multiple relationships at the same time
MATCH (a:Person), (m:Movie), (p:Person)
WHERE a.name = 'Liam Neeson' AND
      m.title = 'Batman Begins' AND
      p.name = 'Benjamin Melniker'
CREATE (a)-[:ACTED_IN]->(m)<-[:PRODUCED]-(p)
RETURN a, m, p

// alternative way of setting properties
MATCH (a:Person), (m:Movie)
WHERE a.name = 'Katie Holmes' AND m.title = 'Batman Begins'
CREATE (a)-[rel:ACTED_IN {roles: ['Rachel','Rachel Dawes']}]->(m)
RETURN a.name, rel, m.title

// test if relationship already exists
// note: you can also use MERGE instead of CREATE to prevent duplicates
MATCH (a:Person),(m:Movie)
WHERE a.name = 'Christian Bale' AND
      m.title = 'Batman Begins' AND
      NOT exists((a)-[:ACTED_IN]->(m))
CREATE (a)-[rel:ACTED_IN]->(m)
SET rel.roles = ['Bruce Wayne','Batman']
RETURN a, rel, m
```

Merging relationships

```
MATCH (p:Person), (m:Movie)
WHERE p.name = 'Robert Zemeckis' AND m.title = 'Forrest Gump'
MERGE (p)-[:DIRECTED]->(m)

MATCH (p:Person), (m:Movie)
WHERE p.name IN ['Tom Hanks','Gary Sinise', 'Robin Wright']
      AND m.title = 'Forrest Gump'
MERGE (p)-[:ACTED_IN]->(m)

MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie)
WHERE m.title = 'Forrest Gump'
SET rel.roles =
CASE p.name
  WHEN 'Tom Hanks' THEN ['Forrest Gump']
  WHEN 'Robin Wright' THEN ['Jenny Curran']
  WHEN 'Gary Sinise' THEN ['Lt. Dan Taylor']
END
```

Set property on relationship for node pair dependent on node property:

```
MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie)
WHERE m.title = 'Forrest Gump'
SET rel.roles =
CASE p.name
  WHEN 'Tom Hanks' THEN ['Forrest Gump']
  WHEN 'Robin Wright' THEN ['Jenny Curran']
  WHEN 'Gary Sinise' THEN ['Lieutenant Dan Taylor']
END

MATCH (p1:Person)-[rel:HELPED]->(p2:Person)
WHERE p1.name = 'Tom Hanks' AND p2.name = 'Gary Sinise'
SET rel.research = 'war history'
```

Modify a relationship:

```
MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie)
WHERE m.title = 'Forrest Gump' AND p.name = 'Gary Sinise'
SET rel.roles =['Lt. Dan Taylor']
```

Remove the relationship property

```
MATCH (p1:Person)-[rel:HELPED]->(p2:Person)
WHERE p1.name = 'Tom Hanks' AND p2.name = 'Gary Sinise'
REMOVE rel.research
```

Delete a relationship

```
MATCH (:Person)-[rel:HELPED]-(:Person)
DELETE rel
```

Check no relationships exist from or to node

```
MATCH (a:Person)-[rel]-()
WHERE a.name = 'Katie Holmes'
RETURN count(rel) AS `Number of Katie Holmes relationships:`
```

## Both nodes and relationship

Create a node and a relationship at the same time:

```
MATCH (m:Movie)
WHERE m.title = 'Batman Begins'
CREATE (a:Person)-[:ACTED_IN]->(m)
SET a.name = 'Gary Oldman', a.born=1958
RETURN a, m

// You could do this, YOU NEVER SHOULD!
MERGE (p:Person {name: 'Robert Zemeckis'})-[:DIRECTED]->(m {title: 'Forrest Gump'})
```

## DELETE EVERYTHING

```
MATCH (n) DETACH DELETE (n)
```

## Constraints

Nodes **can** have unique property/set of property values or ensure properties exist.

Relationships **cannot** have unique property/set of property values.

```
// Say a property value must be unique for all nodes of that label:
CREATE CONSTRAINT PersonNameUniqueConstraint ON (p:Person) ASSERT p.name IS UNIQUE

// a property must exist for all nodes of that label
CREATE CONSTRAINT PersonBornExistsConstraint ON (p:Person) ASSERT exists(p.born)

// for each relationship ACTED_IN the property roles MUST BE defined
CREATE CONSTRAINT ActedInRolesExistConstraint ON ()-[r:ACTED_IN]-() ASSERT exists(r.roles)

// add a node key which makes unique tuple on a set of properties
CREATE CONSTRAINT MovieTitleReleasedConstraint ON (m:Movie) ASSERT (m.title, m.released) IS NODE KEY
```

If you want to set a constraint that a node must have a property, you can use a query like this to set any nodes that do not have a default value yet, otherwise trying to add your constraint will fail.

```
// Set default value for any nodes missing a property
MATCH (p:Person)
WHERE NOT exists(p.born)
SET p.born = 0
```

Drop constraints

```
DROP CONSTRAINT MovieTitleConstraint
```

## Indexes

```
// create an index
CREATE INDEX PersonBornIndex FOR (p:Person) ON (p.born)

// create composite key, does not enforce uniqueness!
CREATE INDEX MovieReleasedVideoFormat FOR (m:Movie) ON (m.released, m.videoFormat)

// drop an index
DROP INDEX PersonBornIndex
```


### Full Text Search

* Node or relationship properties.
* Single property or multiple properties.
* Single or multiple types of nodes (labels).
* Single or multiple types of relationships.

```
// create full text search index
CALL db.index.fulltext.createNodeIndex('MovieTaglineFTIndex',['Movie'], ['tagline'])
```

