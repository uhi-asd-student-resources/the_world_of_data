# Graph Databases

Implemented using Neo4j.

# Why Graph a database?

You can represent graphs in traditional RDBMS databases relatively easily but the SQL to access these can become overly complicated.  Graph databases have their relationships as first class citizens, and just like having functions as first class citizens can be useful, so making the relationships explicity can be very useful and ease many types of analytics.  Most applications can be rewritten to be a graph style problem (see NP-hard problems), as always we try to let the representation help us solve the problem.

## Installation

* To learn, install Neo4j server in a Ubuntu virtual machine locally.
* Install Neo4j Desktop to interact with the Neo4j server.
* You can use cyber-shell from the command line.

When you install the server ensure that it has been enabled:
```
sudo systemctl enable neo4j
sudo systemctl start neo4j
```

In a new terminal type:

```
$ cypher-shell        # use username: neo4j, password: neo4j, it will prompt you to set this on first login
```

When opening Neo4J Desktop for the first time you will want to run the load-movies.cypher that should be visible in the Neo4j Primer Project front page. 

## Drivers

You can get python and R packages for interacting with a Neo4j database.

* https://neo4j.com/developer/r/
* https://neo4j.com/developer/python/
* https://neo4j.com/developer/javascript/

## Terminology

**Property** a key value pair that lives either on a node or a property.  They can be strings, numbers, dates, arrays and more.

**Nodes** are the objects/vertices and can have labels (IS-A) and properties (HAS-A).  Nodes should be NOUNS.  Adjectives are the properties of nodes.

```
Judy IS-A person.   # Here Judy has the label "Person"
Judy HAS-A name.    # Here name: "Judy" is a property of a person node.
```

**Relationships** are the edges between the nodes and can have types (actions) and properties (constraints on the actions). Relationships should be VERBS. Adverbs are the properties of relationships.

```
Fred and Judy are friends.  # Here Fred and Judy are nodes and IS-A-FRIEND is the action.
Fred and Judy are in the car since 2pm.  # Fred and Judy are nodes, IN-THE-CAR is the action and SINCE 2pm is the constraint or edge property.
```

**Walk** is a set of nodes and relationships, both of which can appear more than once or not at all, that describe a traversal of the graph.

**Trail** is a walk that has the property than no relationship appears more than once, nodes can appear more than once.

**Path** is a walk that has the property than no node appears more than once, relationshps can appear more than once.

**Cycle** is where the starting node in the traversal is the same as the ending node.

**Longest path** or **Hamiltonian path** visits every node in a graph.  As with all paths, no node can appear more than once in the traversal.

**Cypher** is the query language used by Neo4j.  It is an open standard at [opencypher.org](opencypher.org).  It takes inspiration from SQL and other places.  It is DECLARATIVE, which means you focus on what you want, not how it does it - just like SQL.

**Schema** refers to the information about the nodes, labels and relationships.  In a similar fashion to a RDBMS schema refers to the tables and entity relationships.





