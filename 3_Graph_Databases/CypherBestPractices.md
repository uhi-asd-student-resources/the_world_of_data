# Cypher Best Practices

1. Create Nodes THEN create relationships
2. Use MERGE rather than CREATE to create relationships to stop duplication.
3. Create and use indexes effectively.
4. Use parameters rather than literals in your queries.
5. Specify node labels in MATCH clauses.
6. Reduce the number of rows passed processed.
7. Aggregate early in the query, rather than in the RETURN clause, if possible.
8. Use DISTINCT and LIMIT early in the query to reduce the number of rows processed.
9. Defer property access until you really need it.

