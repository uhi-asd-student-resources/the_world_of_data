# Importing Data into Neo4j

Before importing you should have a common schema.  Import can be done using JSON, CSV or XML.

* Names of entities (node labels).
* Names of relationships.
* Names of properties for nodes and relationships.
* Constraints to be defined.
* Indexes required.
* Identify the most important queries

Read https://neo4j.com/graphacademy/training-intro-40/17-using-load-csv-import/ before importing to check requirements and to plan a good route for the transformation.

## Tips

* Create constraints before uploading data.
* Create any additional indexes after the data has been uploaded.


## Doing the import

Look at your requirements before importing as different methods will have different performance characteristics and the amount of work may vary.

1. Load Comma-Separated Value File (CSV) e.g. Excel export
2. APOC + Cypher (+ JDBC)
3. Neo4j ETL tool
4. ./neo4j-admin import with CSV file Fastest)
5. Script of your choice with Bolt

## Load CSV

* CSV files that have been extracted from an RDBMS.
* Neo4j Browser or Cypher-shell.
* Neo4j database running locally, in Neo4j Aura, in Neo4j Sandbox.
* Optionally using a Neo4j Cluster.
* Special handling if > 100K lines of data.

## APOC and Cypher (+ JDBC)

* CSV, XML, or JSON files that have been extracted from an RDBMS.
* Neo4j Browser or Cypher-shell.
* Neo4j database running locally, in Neo4j Aura, in Neo4j Sandbox.
* Optionally using a Neo4j Cluster.
* No limit to size of data to import.
* You can also use APOC’s jdbc connection features to access a live RDBMS. 

```
// check for APOC plugin
CALL dbms.procedures()
YIELD name WHERE name STARTS WITH "apoc"
RETURN name
```

Clear a graph completing between import attempts:

```
// Delete all constraints and indexes
CALL apoc.schema.assert({},{},true);
// Delete all nodes and relationships
CALL apoc.periodic.iterate(
  'MATCH (n) RETURN n',
  'DETACH DELETE n',
  { batchSize:500 }
)
```

```
// or
CREATE OR REPLACE DATABASE my_new_database_name 
```

# Neo4j ETL 

* Neo4j Desktop with ETL tool installed.
* Neo4j database running locally, in Neo4j Aura, in Neo4j Sandbox.
* Optionally using a Neo4j Cluster.
* Live RDBMS Server.
* No limit to size of data to import.

## Neo4j-admin

* CSV files that have been extracted from an RDBMS.
* Neo4j database running locally.
* No limit to size of data to import.

## Your language, drivers and Bolt

* Neo4j Browser or Cypher-shell.
* Neo4j database running locally, in Neo4j Aura, in Neo4j Sandbox.
* Optionally using a Neo4j Cluster.
* Live RDBMS Server.
* Application responsible for transaction scoping.
* No limit to size of data to import.



## Reference

https://neo4j.com/graphacademy/training-intro-40/16-overview-importing-data/

