# Cypher Language Cheet Sheet

// begin a comment

(*node*)                      refers to zero or more nodes

(```<variable>```:```<label>```)        declare a new variable that IS-A label

(:*label*)                    nodes of type Label

(*n*:*label*)                   refer to node n of type Label

```
// anonymous node not be referenced later in the query
()
// variable p, a reference to a node used later
(p)
// anonymous node of type Person
(:Person)
// p, a reference to a node of type Person
(p:Person)
// p, a reference to a node of types Actor and Director
(p:Actor:Director)
```

Show the current database schema:

```
CALL db.schema.visualization()          # show the nodes, labels and relationships
CALL db.propertyKeys()                  # show the property keys in the database
CALL db.constraints()                   # show the constraints
CALL db.indexes()                       # show indexes
CALL dbms.listQueries()                 # show current queries
```

Return all nodes of type Person

```
MATCH(p:Person)
RETURN p
```

Filter nodes by property

```
MATCH (n:Label {property-key:value}) RETURN n 
```

Filter nodes and return a particular property, in this case .title and .released.  You can use AS (just like in SQL) to alias the column to a new title.

```
MATCH (n:Label {property-key:value}) RETURN n.title AS `Movie Title`, n.released as `Released Date`
```

Traverse the graph using (node)-[:relationship]->(node):

```
MATCH (p:Person)-[:WROTE]->(:Movie {title: 'Speed Racer'}) RETURN p.name
```

Traverse the graph without specifying the exact relationship between two types of node:

```
MATCH (m:Movie)<--(:Person {name: 'Tom Hanks'}) RETURN m.title
MATCH (:Person {name: 'Tom Hanks'})-->(m:Movie) RETURN m.title
````

Return the type information for the relationship:

```
MATCH (m:Movie)-[rel]-(:Person {name: 'Tom Hanks'}) RETURN m.title, type(rel)
MATCH (p:Person)-[rel]-(m:Movie {title: 'Forrest Gump'}) RETURN p, rel, m
MATCH (m:Movie)-[rel:ACTED_IN]-(:Person {name: 'Tom Hanks'}) RETURN m.title, rel.roles
```

Filter by node property using WHERE clause (not you can do the first query using the {} syntax in the node variable, but the second you cannot):

```
MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WHERE a.name = 'Tom Cruise'
RETURN m.title as Movie

MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WHERE a.born > 1960 AND m.title = 'The Matrix'
RETURN a.name as Name, a.born as `Year Born`

// note here we test the node label using the m:Movie syntax
MATCH (m)
WHERE m:Movie AND m.released = 2000
RETURN m.title

// use type(rel) to get the type of the relationship
MATCH (a)-[rel]->(m)
WHERE a:Person AND type(rel) = 'WROTE' AND m:Movie
RETURN a.name as Name, m.title as Movie
```

Testing existence of a property as not all nodes need have the same property set.  Here a.born returns *null* keyword.

```
MATCH (a:Person)
WHERE NOT exists(a.born)
RETURN a.name as Name, a.born
```

Use relationships to filter results using NOT (expression):

```
MATCH (a:Person)-[:PRODUCED]->(m:Movie)
WHERE NOT ((a)-[:DIRECTED]->(:Movie))
RETURN a.name, m.title
```

Test for no labels on node

```
MATCH (m)
WHERE m.title = 'Forrest Gump' AND labels(m) = []
DETACH DELETE m
```

A query can have multiple MATCH patterns and also constaint the result not by having multiple input nodes specified.

```
MATCH (a:Person)-[:ACTED_IN]->(m:Movie)<-[:DIRECTED]-(d:Person),
WHERE a.name = 'Gene Hackman'
RETURN m.title as movie, d.name AS director 

MATCH (a:Person)-[:ACTED_IN]->(m:Movie)<-[:DIRECTED]-(d:Person),
      (a2:Person)-[:ACTED_IN]->(m)
WHERE a.name = 'Gene Hackman'
RETURN m.title as movie, d.name AS director , a2.name AS `co-actors`

// optional match clause, similar to HAVING in SQL where it operates
// on the dataset produced after the initial WHERE clause
MATCH (p:Person)
WHERE p.name STARTS WITH 'Tom'
OPTIONAL MATCH (p)-[:DIRECTED]->(m:Movie)
RETURN p.name, m.title
```

Ignore direction of the relationship using -[]-, in this case either following or followed by "James Thompson".

```
MATCH (p1:Person)-[:FOLLOWS]-(p2:Person)
WHERE p1.name = 'James Thompson'
RETURN p1, p2
```

Allow for exactly N hops of the same relationship:

```
MATCH (p1:Person)-[:FOLLOWS*3]-(p2:Person)
WHERE p1.name = 'James Thompson'
RETURN p1, p2

// between 1 and 3 hops inclusive
MATCH (p1:Person)-[:FOLLOWS*1..3]-(p2:Person)
WHERE p1.name = 'James Thompson'
RETURN p1, p2

// 1 or more hops away
MATCH (p1:Person)-[:FOLLOWS*]-(p2:Person)
WHERE p1.name = 'James Thompson'
RETURN p1, p2

// Find the shortest path between two nodes
MATCH p = shortestPath((m1:Movie)-[*]-(m2:Movie))
WHERE m1.title = 'A Few Good Men' AND
      m2.title = 'The Matrix'
RETURN  p
```

Show the subgraph that matches a pattern. 

```
MATCH paths = (m:Movie)-[rel]-(p:Person)
WHERE m.title = 'The Replacements'
RETURN paths
```

Use WITH to do intermediate processing which you can then access in the WHERE clause:

```
MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WITH  a, count(m) AS numMovies, collect(m.title) AS movies
WHERE numMovies = 5
RETURN a.name, movies
```

Use the **size** function to get how many nodes fit a relationship, you can think of OPTIONAL MATCH as a left join, which will return NULL if the new relationship does not exist.:

```
// size can be used to get the size of a set of matching nodes
MATCH (m:Movie)
WITH m, size((:Person)-[:DIRECTED]->(m)) AS directors
WHERE directors >= 2
OPTIONAL MATCH (p:Person)-[:REVIEWED]->(m)
RETURN  m.title, p.name

// size can also be used to get the length of an array
MATCH (p:Person)-[:ACTED_IN]->(m:Movie)
WITH p, collect(m) AS movies
WHERE size(movies)  > 5
RETURN p.name, movies
```

You can use multiple WITH clauses to do processing before and after the WHERE clause.  
```
MATCH (p:Person)-[:ACTED_IN]->(m:Movie)
WITH p, collect(m) AS movies
WHERE size(movies)  > 5
WITH p, movies UNWIND movies AS movie
RETURN p.name, movie.title
```

Using COLLECT to act as GROUP BY:

```
// here the COLLECT on all but 1 RETURN fields acts as group by.
MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WHERE m.released >= 1990 AND m.released < 2000
RETURN  m.released, collect(m.title), collect(a.name)

// here the COLLECT on all but 1 RETURN fields acts as group by.
MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WHERE m.released >= 1990 AND m.released < 2000
RETURN  m.released, collect(DISTINCT m.title), collect(a.name)
```

Use DISTINCT on a node to remove duplication

```
// distinct title and release date
MATCH (p:Person)-[:DIRECTED | ACTED_IN]->(m:Movie)
WHERE p.name = 'Tom Hanks'
RETURN DISTINCT m.title, m.released

// distinct released and title combinations
MATCH (p:Person)-[:DIRECTED | ACTED_IN]->(m:Movie)
WHERE p.name = 'Tom Hanks'
WITH DISTINCT m
RETURN m.released, m.title
```

Order results using ORDER BY

```
MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WHERE m.released >= 1990 AND m.released < 2000
RETURN  m.released, collect(DISTINCT m.title), collect(a.name)
ORDER BY m.released DESC
```

Limit the results

```
MATCH (:Person)-[r:REVIEWED]->(m:Movie)
RETURN  m.title AS movie, r.rating AS rating
ORDER BY r.rating DESC LIMIT 5

// do pagination of results using SKIP...LIMIT, similar to LIMIT start,n in SQL.
MATCH (:Person)-[r:REVIEWED]->(m:Movie)
RETURN  m.title AS movie, r.rating AS rating
ORDER BY r.rating DESC SKIP 1 LIMIT 5
```

Get information about any relationship type:

```
// here we get all nodes joined by a HELPED relationship 
// and return the person.name property values plus the
// relationship
MATCH (p1:Person)-[rel:HELPED]-(p2:Person)
RETURN p1.name, rel, p2.name
```

Find existing relationships

```
MATCH (p {name: 'Michael Caine'})-[*0..1]-(m)
RETURN p, m
```

Manipulating Dates

```
RETURN date().day, date().year, datetime().year, datetime().hour,
       datetime().minute
```

Manipulating Timestamps

```
RETURN datetime({epochmillis:timestamp()}).day,
       datetime({epochmillis:timestamp()}).year,
       datetime({epochmillis:timestamp()}).month
```

String Functions

* toLower

WITH/RETURN Functions

* UNWIND, converts a JSON array to individual items (the opposite of COLLECT)
* COLLECT, converts results into a JSON array (the opposite of UNWIND)
* COUNT, count and returns the result (equivalent to *size(collect())*)
* {}, return a JSON map of kay-value pairs

```
MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WHERE a.name = 'Tom Hanks'
RETURN  m {.title, .released}
```

* DATE() returns todays date object, e.g. date().year gives the current year

Operators

* CONTAINS
* STARTS WITH
* =~ for testing regular expressions

IN  tests if a value is in an array

```
MATCH (m:Movie)
WHERE m.released IN [2000, 2004, 2008]
RETURN m.title, m.released
```


## Full Text Searches

Make a query against our full text index

```
CALL db.index.fulltext.queryNodes('MovieTaglineFTIndex', 'real OR world') YIELD node
RETURN node.title, node.tagline

// retrieve a Lucene score which tells you how good the match was
CALL db.index.fulltext.queryNodes('MovieTaglineFTIndex', 'real AND world') YIELD node, score
RETURN node.title, node.tagline, score
```

## References

* https://neo4j.com/graphacademy/training-intro-40/03-introduction-to-cypher/