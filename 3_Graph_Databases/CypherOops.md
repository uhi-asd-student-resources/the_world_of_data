# Cypher Oops

## Killing a long query

Assumes you have a session A with long running query, you open a new Neo4j Browser session called Session B.

* You can close the result pane in Session A, if the query can be seen in Session B.
* You can kill any running query seen in Session B.
* You can close the Neo4j Browser that is running Session A.
