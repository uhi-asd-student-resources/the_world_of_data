# Using bash on the command line

* Complete an introduction to the linux command line if you have not already done so

This is a compilation of common tips and tricks to get you started on scripting.

## Use scripting to write a longer command in awk, sed, R

Use the hash-bang (#!) syntax for that utility

```
#!/bin/bash
```

```
#!/usr/bin/python
```

```
#!/bin/awk -f
```

```
#!/bin/Rscript
```

## Get the path of the current script

```
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
```
