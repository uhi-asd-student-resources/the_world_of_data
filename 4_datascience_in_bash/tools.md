# Tools

## View environment variables and settings
* env
* set
* export - make variable available to child processes

## Terminal/Scripting tools

* echo
* test

## Bash special parameters

[Bash special parameters help page](https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html)

* $?
* $!
* $@
* $#
* $0

## Bash pipelines and process control

* [Bash pipelines](https://www.gnu.org/software/bash/manual/html_node/Pipelines.html)
* [Learn Linux, 101: Streams, pipes, and redirects](https://developer.ibm.com/technologies/linux/tutorials/l-lpic1-103-4/)

Should cover the following:
* \>
* <
* \>\>
* 2\>&1
* |
* &
* bg/ctrl+z
* fg

## Basic file manipulation

* cat
* head
* tail
* less
* more
* grep
* wc
* join

## More complex string matching
* awk
* sed
* jq    (needs jq)
* perl -e
* egrep -o
* cut
* touch
* tr
* diff
* hexdump

## Data manipulation
* uniq
* sort

## Utility map utilities
* xargs
* paste

## Diskspace utilities
* df
* du

## Output redirection
* tee

## Web tools
* wget
* lynx
  
## Process utilities
* timeout
* time

## File compression
* tar
* zip/unzip

## File Metadata
* file
* stat

## Hash Functions
* sha1sum/md5sum etc

## RPA
* Rscript
* expect
* getopt/getopts

## File conversion tools
* tesseract