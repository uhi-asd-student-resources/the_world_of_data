# Regressions
# Reference: http://www.sthda.com/english/articles/40-regression-analysis/168-multiple-linear-regression-in-r/
# Simple regression has 1 input and 1 output
# Multivariate regression has N inputs and 1 output

library(tidyverse)

data("marketing", package = "datarium")
head(marketing, 4)

# lm is a very useful function for linear modelling
model <- lm(sales ~ youtube + facebook + newspaper, data = marketing)
print(summary(model))

# Look at F-statistics, should be towards 0 to be significant
# At least one of the predictor variables is related to the output
# Examine coefficients to see which one.
# The t-statistic further from zero shows which one or more is significant.

# Remove newspaper  as its not significant:

model  <- lm(sales ~ youtube + facebook, data = marketing)
print(summary(model))

# The estimate column as our weightings for each factor
# e.g. sales = 3.5 + 0.046*youtube + 0.188 * facebook

# the following shows the confidence interval in the values given
print(confint(model))