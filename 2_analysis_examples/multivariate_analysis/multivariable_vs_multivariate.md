# What is the difference between Multivariable and Multivariate regression models?

Multivariable is where there are multiple explanatory variables involved.

Multivariate is where you have the same variable measured the same individual over multiple time points (repeated measures). This tends to be used in longitudinal studies.  Here there are multiple dependent variables on the left hand side of the regression equation for a single predictor variable.