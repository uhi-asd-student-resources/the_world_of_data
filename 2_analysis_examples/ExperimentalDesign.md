# Experimental Design

Materials:
* https://www.linkedin.com/learning/the-data-science-of-experimental-design/conducting-an-experiment-online?u=56746241
* https://www.linkedin.com/learning/six-sigma-green-belt/introduction-to-design-of-experiments?u=56746241

## Hypothesis

* $H0$ is the null Hypothesis
* $H1$ is the primary hypothesis

The goal of an experiment is to provide evidence that there is a difference between the primary hypothesis the null hypothesis.  You can think of the null hypothesis as "no change" or the "status quo".  

Good null hypotheses:

* When I change how much soup is in the tin, there will be no change to how long it takes to boil.
* When I change the learning rate from 0 to 0.1, there will be no change in how long the optimisation will take.

## Tails?

A primary hypothesis that only seeks a difference between the null and primary hypothesis is called a 'two-tailed test'.  

Examples:
* When I change how much soup in in tin, there will be a change to how long it takes to boil.
* When I change the learning rate from 0 to 0.1, there will be a change in how long the optimisation will take.

A primary hypothesis that seeks a one sided result, such as greater than or less than, from the null hypothesis is called a 'one-tailed test'.

Examples:
* When I change how much soup in in tin, it will take more time to boil.
* When I change how much soup in in tin, it will take less time to boil.
* When I change the learning rate from 0 to 0.1, the optimisation will take more time.
* When I change the learning rate from 0 to 0.1, the optimisation will take less time.

## Degrees of freedom

The degrees of freedom is the size of the choice your experiment has.  It is calculated as:

$$df = number_of_categories - 1$$

Example: If you are selecting from 3 price options, your degrees of freedom is 2.

*In parametric test this is the number of rows - 1.*

# Handling errors

* Type I error ($\alpha$ , also called significance level): the probability to reject $H_0$ (the null hypothesis) when it is true. (False positive)
* Confidence level ($1 - \alpha$) : ability to produce accurate intervals that include the true parameter value if many samples were to be generated
* Type II error ($\beta$): the probability to FAIL to reject $H_0$ when it is false.(False negative)
* Power of the statistical test ($1- \beta$) :the probability to reject H₀ when it is false

# Parameteric vs Non-parametric

**Parametric** tests assume underlying statistical distributions in the data.   If the mean more accurately represents the center of the distribution of your data, and your sample size is large enough, use a parametric test.  In this type of test you can normally assume your data is Normally distributed.

**Non-parametric** tests do not rely on any distribution. Used when tests show the data does not obey the statistical distribution requirements. If the median more accurately represents the center of the distribution of your data, use a nonparametric test even if you have a large sample size.  Here your data is not normally distributed.  Non-parametric tests tend to be regarded as less *powerful* as they use less data and do not obey well defined distributions.  Where possible you should try to use parametric tests.

## Training

## Validation

## Test

## k-fold Cross Validation

