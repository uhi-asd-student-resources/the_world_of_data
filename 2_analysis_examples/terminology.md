# Terminology

**Classification** is similar to clustering except that the classes are predefined to which items are assigned.

**Clustering**: separating a set of points (your input data) into groups of similar points such that those with the same output value will map to those in a particular group or cluster.  Similar to classification except the classes are defined by the data.

**Expectation Maximisation**: a set of algorithms that have 2 steps in common, a measure of distance which allocates the group, and an update method that minimises the error, thereby maximising the accuracy.

**Mixture model**: this is basically the same as a group of clusters.  It is a way to combine small sets of points that might have different distributions into one mathematical formulation.

## Matrix terminology

When dealing with multiple characteristics in data we tend to think in terms of matrices.  Here are some properties you will want to know about.

**Determinant** of a matrix: given a square matrix, this can be thought of as the volume of the shape drawn out by the vectors in the matrix. See [MathsIsFun](https://www.mathsisfun.com/algebra/matrix-determinant.html) for more information.

**Eigenvalue** the scaling factor that we use with eignvectors.  Eigenvalues is the scale of the stretch that allows our eigenvectors to match our original matrix of data.  It is a single number.

**Eigenvector** are the set of vectors that will not change under rotation or other transformation.  We can use these as a core set of axis, a bit like the x and y axis you are used to, to describe our data.

Both eigenvalues and eigenvectors can be real or complex values.

## Random Variable terminology

In one dimension we need to know the following attributes:

$X$ this is the name of our set of data points (dataset) normally, you can think of these are our inputs.  It looks like a spreadsheet of data.  A point in the set $X$ is called $X_i$ or $x_i$, this is also called an observation.

**N** is the number of rows or samples we have in our dataset.

**mean** ($\mu$ or $\bar{X}$) is the central point of a set of data points.

**Variance** (commonly known as *var*) of a variable is a measure of how far data is spread from a mean value - a measure of dispersion.

$$\sigma^{2} = \frac{1}{N} * \Sigma(X-\mu)^2$$

**Standard Deviation** (commonly known as *stdev*) is a measure of spread - how much data is concentrated around the mean.

$$\sigma = \sqrt(\sigma^2)$$

There is also **skew** and **kurtosis** that can tell us about where the data is in relation to the mean and how quickly the peak dies away. 

Moving to 2 or more variables requires us to deal with:

**Correlation** (*corr*) is a measure of dependency between variables X and Y (aka $X_1$, $X_2$, ...).  A positive correlation means the variables move in the same direction, a negative correlation means the variables move in opposite directions and a correlation of zero means the variables have no relationship.  It measures dependency not the amount of change.

$$ Correlation = \frac{Covariance(X,Y)}{\sigma_x * \sigma_y}$$

**Covariance** (*cov*) is a measure of variation between 2 or more random variables.  It evaluates how much the variables change together.  It does not measure the dependency between variables.  Positive covariance means that the variables tend to move in the same direction, negative covariance means that the variables tend to move in opposite directions.  

$$Covariance(X,Y) = \frac{\Sigma^{n}_{i=1}(X_i-\bar{X})(Y_i-\bar{Y})}{n-1}$$

Notice that the similarity betwen variance and the covariance.
