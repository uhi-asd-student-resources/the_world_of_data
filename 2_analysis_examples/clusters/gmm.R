# Gaussian Mixture Method example
#
# This is similar to kmeans but instead of using just means we use a gaussian (3d normal distribution) to cover similar points.
#
# References
# * https://towardsdatascience.com/mixture-modelling-from-scratch-in-r-5ab7bfc83eef
#

# load Iris dataset
library(datasets)
data(iris)

# Use first four columns as input
X <- iris[,1:4]
# use 5th as output
y <- iris[,5]
# set colours for the 3 values of y
y_col <- c('#7DB0DD', '#86B875', '#E495A5')
# write out to pdf
pdf('dat_iris.pdf')
# draw a matrix of scatterplots
pairs(X, lower.panel = NULL, col = y_col[y])
par(xpd = T)    # clip all plotting to figure region (This is smaller than the plot region)
legend(x = 0.1, y = 0.4, legend = as.character(levels(y)), fill = y_col)
dev.off()


# Uses EM algorithm with multivariate normal distribution to estimate cluster probability
# @param Sigma      this is a square covariance matrix 
# @return matrix
mvnorm.cov.inv <- function(Sigma) {
  # this function calculates the value called the "inverse covariance matrix of a multivariate guassian"
  # covariance is a measure of the extent to which the corresponding elements in 2 sets of ordered data move in the same direction.
  # reference: https://stattrek.com/matrix-algebra/covariance-matrix.aspx
  
  E <- eigen(Sigma)                 # calculate the "eigenvalues" and "eigenvectors" of the matrix
                                    # Why? because we want to have a way of representing change that removes transformations
                                    # if Sigma is N dimensional we get N values and N vectors
  Lambda.inv <- diag(E$values^-1)   # create a diagonal matrix with each value being 1/eigenvalue for that dimension
  eigenvectors <- E$vectors                    
  return(eigenvectors %*% Lambda.inv %*% t(eigenvectors)) 
}

# multivariate Gaussian probability distribution function (pdf)
# this is a normal distribution but in higher dimensions
# you can find the formula for this in https://en.wikipedia.org/wiki/Multivariate_normal_distribution under
# density function.
mvn.pdf.i <- function(datapoint, mean, covariance_matrix)
  1/sqrt( (2*pi)^length(datapoint) * det(covariance_matrix) ) *                 # det = determinant of a matrix
  exp(-(1/2) * t(datapoint - mean) %*% mvnorm.cov.inv(covariance_matrix)        # exp = exponent e.g. e^x, where x is some number
                                                                                # t = is the transpose function (switches rows for columns)
  %*% (datapoint - mean)  )                                                     # remember %x% is matrix multiplication operator

# this is just looping mvn.pdf.i over each row X[i,]
mvn.pdf <- function(X, mu, covariance_matrix)
  apply(X, 1, function(xi) mvn.pdf.i(as.numeric(xi), mu, covariance_matrix))


gmm.fromscratch <- function(X, num_clusters){
  p <- ncol(X)  # number of parameters
  n <- nrow(X)  # number of observations

  
  Delta <- 1; iter <- 0; itermax <- 30

  # initialise our clusters using kmeans
  # if you have not looked at kmeans - do that now
  km.init <- kmeans(X, num_clusters)
  mu <- km.init$centers
  mu_mem <- mu            # this is just our remembering our previous value
  # we need to normalise our data by calculating the proportion of items are in each cluster
  w <- sapply(1:num_clusters, function(i) length(which(km.init$cluster == i)))
  w <- w/sum(w)
  print("initial weights")
  print(w)

  # calculate our initial covariance matrix (PxPxN)
  # we use array rather than matrix as it is a 3 dimensional object
  cov <- array(dim = c(p, p, num_clusters))
  # populate our 3d structure
  for(i in 1:p) 
    for(j in 1:p) 
      for(c in 1:num_clusters)     
        # the 1/n rather than 1/n-1, this is the difference beween population and sample 
        # very often we just use 1/n as its easier.
        cov[i, j, c] <- 1/length(km.init$cluster == c) * sum((X[km.init$cluster == c, i] - mu[c, i]) * (X[km.init$cluster == c, j] - mu[c, j]))

  # now we optimise our clusters
  while(Delta > 1e-4 && iter <= itermax){    
    print(paste("iteration:", iter, "error:", Delta))
    # E-step (like kmeans we have an E and a M step)
    # here we are finding which gaussian best describes each point
    mvn.c <- sapply(1:num_clusters, function(c) mvn.pdf(X, mu[c,], cov[,, c]))
    print("multivariate guassian clusters")
    print(dim(mvn.c))     # 150 x 3
    print(head(mvn.c))    # what this has is values very close to 0 and then a large number in the cluster that matches the data
    # we weight our results by how much data we have for each cluster
    # here we are forcing the values to between 0 and 1
    # the transposes are required to ensure we can do the multiplication as these are matrices of values.
    r_ic <- t(w*t(mvn.c)) / rowSums(t(w*t(mvn.c)))
    print("new cluster assignment")
    print(dim(r_ic))
    print(head(r_ic))
    
    # M-step
    # here we move our clusters to a place that is a better representation of our data.
    n_c <- colSums(r_ic)  # how many items in each cluster
    print("number of items in each cluster")
    print(n_c)
    w <- n_c/sum(n_c)     # recalculate our weights
    print("new weights:")
    print(w)

    # recalculate our means
    mu <- t(sapply(1:num_clusters, function(c) 1/n_c[c] * colSums(r_ic[, c] * X)))
    print("new means:")
    print(mu)

    # recalculate our covariance matrix
    for(i in 1:p) 
      for(j in 1:p) 
        for(c in 1:num_clusters) 
          cov[i, j, c] <- 1/n_c[c] * sum(r_ic[, c] * (X[, i] - mu[c, i]) * r_ic[, c] * (X[, j] - mu[c, j]))    
    
    # update our error and our iteration counter
    Delta <- sum((mu - mu_mem)^2)
    iter <- iter + 1
    mu_mem <- mu
  }

  cluster_assignments <- apply(r_ic, 1, which.max)              # for each row get index of largest value
  return(list(softcluster = r_ic, cluster = cluster_assignments))
} 


# run GMM
gmm <- gmm.fromscratch(X, 3)

# like in kmeans we can draw out our clusterings
pairs(X, lower.panel = NULL, col = gmm$cluster)
table(y, gmm$cluster)

# what is our accuracy?
expected <- matrix(c(0,0,50,0,50,0,50,0,0),nrow=3)
observed <- table(y, gmm$cluster)
diff_matrix <- (observed - expected)
diff_matrix[which(diff_matrix<0)] <- 0
error <- sum(diff_matrix) / nrow(X)
accuracy <- 1 - error

# again we have a function we can use for this:
library(mclust)
gmm.readymade <- Mclust(X,3)
table(y, gmm.readymade$classification)
observed <- table(y, gmm.readymade$classification)
diff_matrix <- (observed - expected)
diff_matrix[which(diff_matrix<0)] <- 0
error.readymade <- sum(diff_matrix) / nrow(X)
accuracy.readymade <- 1 - error

print(paste("accuracy:", accuracy, "accuracy of library:", accuracy.readymade))