# KMeans example
#
# Assumes we can CLUSTER the data together using a circle (centroid) around the mean of a set of points
#
# References
# * https://towardsdatascience.com/mixture-modelling-from-scratch-in-r-5ab7bfc83eef
#

# load Iris dataset
library(datasets)
data(iris)

# Use first four columns as input
X <- iris[,1:4]
# use 5th as output
y <- iris[,5]
# set colours for the 3 values of y
y_col <- c('#7DB0DD', '#86B875', '#E495A5')
# write out to pdf
pdf('dat_iris.pdf')
# draw a matrix of scatterplots
pairs(X, lower.panel = NULL, col = y_col[y])
par(xpd = T)    # clip all plotting to figure region (This is smaller than the plot region)
legend(x = 0.1, y = 0.4, legend = as.character(levels(y)), fill = y_col)
dev.off()


# finds partition such that squared error between empirical mean
# and points in cluster is minimized over all k clusters
km.fromscratch <- function(X, numClustersRequested){
  p <- ncol(X)  # number of parameters, in our case this will be 4
  n <- nrow(X)  # number of observations, in our case this will be 150
  print(paste("Parameters/Inputs:", p))
  print(paste("Samples/Rows:", n))

  # This is an optimisation step.
  # We keep going until either our error goes below a threshold (minimisation)
  # or we do the loop at most 'itermax' times.
  Delta <- 1; iter <- 0; itermax <- 30
  threshold = 1e-4
  
  # Initialisation step
  centroid <- X[sample(nrow(X), numClustersRequested),]        # get k random numbers between 1 and nrow(X), these are our starting points
  centroid_mem <- centroid

  while(Delta > threshold && iter <= itermax){
    print(paste("iteration:", iter, "error:", Delta))

    # equivalent to E-step (expectation step)
    # this calculates the distance of each point from each centroid
    # this is the same as saying:
    #
    # total_error_matrix = 0                                # this matrix is a column per cluster
    # for( c in 1:numClustersRequested ) {                  # where k is the number of clusters we want remember
    #   for( i in 1:n ) {                                   # where n is the number of rows aka samples
    #       squared_error = (centroid[c,] - X[i,])^2        # we square remember to always have a positive value (could take absolute value )
    #       total_error_matrix[i,c]] += squared_error       # add to total error so far
    #
    d <- sapply(1:numClustersRequested, function(c) sapply(1:n, 
      function(i) sum((centroid[c,] - X[i,])^2) ))
    print("Distance dataframe")
    print(dim(d))   # this is 150 rows x 3 columns, one for each cluster
    print(head(d))
    # find minimum distance for each
    cluster <- apply(d, 1, which.min)   # across each row which column as the minimal distance, collapses matrix to 1 column
    print("Cluster that minimises the sum square error")
    print(dim(cluster))     # this is NULL as we now have a vector rather than a matrix
    print(cluster)

    # equivalent to M-step (maximisation)
    # we move the center of the centred to the mean of the values that have been assigned to the cluster
    # we take the mean of each columns across all rows that are in the same cluster
    centroid <- t(sapply(1:numClustersRequested, function(c) apply(X[cluster == c,], 2, mean)))
    print("new centroid locations")
    print(centroid)
    
    # calculate difference between old centroid and new centroid
    # sum_square again here notice.
    Delta <- sum((centroid - centroid_mem)^2)

    # update our initial values
    iter <- iter + 1; 
    centroid_mem <- centroid
  }
  return(list(centroid = centroid, cluster = cluster))
}# run K-means


numClustersRequested <- 3                       # we pick 3 as we have 3 outputs in our output vector y
km <- km.fromscratch(X, numClustersRequested)
# note here that km is a list of 2 items
# > km
# $centroid     <-- the center of each cluster
#      Sepal.Length Sepal.Width Petal.Length Petal.Width
# [1,]     6.853846    3.076923     5.715385    2.053846
# [2,]     5.883607    2.740984     4.388525    1.434426
# [3,]     5.006000    3.428000     1.462000    0.246000

# $cluster      <-- which cluster each point in the data set is in
#   [1] 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3
#  [38] 3 3 3 3 3 3 3 3 3 3 3 3 3 1 2 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
#  [75] 2 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 1 2 1 1 1 1 2 1 1 1 1
# [112] 1 1 2 2 1 1 1 1 2 1 2 1 2 1 1 2 2 1 1 1 1 1 2 1 1 1 1 2 1 1 1 2 1 1 1 2 1
# [149] 1 2

# Draw this out, colouring our clusters
pairs(X, lower.panel = NULL, col = km$cluster)
# Is there a good relationship between outputs and clusters?
# ideally we want all of each category in y to be in a unique cluster
# if not, this shows us we do not have a perfect fit (which may or may not be possible using this technique)
table(y, km$cluster)

# what is our accuracy?
expected <- matrix(c(0,0,50,0,50,0,50,0,0),nrow=3)
observed <- table(y, km$cluster)
diff_matrix <- (observed - expected)
diff_matrix[which(diff_matrix<0)] <- 0
error <- sum(diff_matrix) / nrow(X)
accuracy <- 1 - error

# Do I really need to do all this coding???
# No, of course not.  Here is a ready made function for you:

km.readymade <- kmeans(X,3)

# lets compare this version against the one above:
table(y, km$cluster)
table(y, km.readymade$cluster)
observed <- table(y, km.readymade$cluster)
diff_matrix <- (observed - expected)
diff_matrix[which(diff_matrix<0)] <- 0
error.readymade <- sum(diff_matrix) / nrow(X)
accuracy.readymade <- 1 - error

# are the accuracies different?  
# Does it matter which output value matches which cluster?