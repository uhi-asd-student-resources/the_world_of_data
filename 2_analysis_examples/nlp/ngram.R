# N-Gram Model (NLP)
# Reference: https://rpubs.com/abhijitjantre/NgramModelWithNaturalLanguageProcessingNLP
# Why? Word prediction based on previous n words
# For this you will need the data from here: https://d396qusza40orc.cloudfront.net/dsscapstone/dataset/Coursera-SwiftKey.zip

library(NLP)
library(tm)                     # this library has most of the document processing functions in
library(fpc)
library(RColorBrewer)
library(wordcloud)
library(ggplot2)
library(stringi)
library(data.table)

# you can also use the package 'ngram'
DATALOCATION <- "/tmp/final/en_US/"
blogs <- readLines(paste(DATALOCATION,"en_US.blogs.txt",sep=""), encoding="UTF-8", skipNul=TRUE)
news <- readLines(paste(DATALOCATION,"en_US.news.txt",sep=""), encoding="UTF-8", skipNul=TRUE)
twitter <- readLines(paste(DATALOCATION,"en_US.twitter.txt",sep=""), encoding="UTF-8", skipNul=TRUE)


blogFileSize <- file.size(paste(DATALOCATION,"en_US.blogs.txt",sep=""))
newsFileSize <- file.size(paste(DATALOCATION,"en_US.news.txt",sep=""))
twitterFileSize <- file.size(paste(DATALOCATION,"en_US.twitter.txt",sep=""))
m <- matrix(c(NROW(blogs),NROW(news),NROW(twitter),sum(nchar(blogs)),sum(nchar(news)),
            sum(nchar(twitter)),(blogFileSize/1024^2),(newsFileSize/1024^2),(twitterFileSize/1024^2)),
            byrow = FALSE,nrow=3,ncol=3,
            dimnames = list(c("blogs","news","twitter"),
                            c("No.Of Lines","No. Of Characters","File Size in Mb")))
Wordcount <- sapply(list(blogs,news,twitter),stri_stats_latex)['Words',]
FullSummary <- cbind(m,Wordcount)
print(FullSummary)

# remove all non-ascii characters and we replace them with an empty string
blogs <- iconv(blogs,"latin1","ASCII",sub = "")
news <- iconv(news,"latin1","ASCII",sub = "")
twitter <- iconv(twitter,"latin1","ASCII",sub = "")

# take off a proportion to use as a training set
training_proportion <- 0.01
blogs.training <- sample(blogs,round(training_proportion*length(blogs)))
news.training <- sample(news,round(training_proportion*length(news)))
twitter.training <- sample(twitter,round(training_proportion*length(twitter)))
TrainingSummary <- matrix(c(NROW(blogs.training),NROW(news.training),NROW(twitter.training)),
                        byrow = TRUE,nrow=3,ncol=1,
                        dimnames = list(c("blogs.training","news.training","twitter.training"),
                                        "No.Of Rows"))
print(TrainingSummary)

# we set the random seed so we can repeat the experiment easily
set.seed(666)

trainingset <- c(blogs.training,news.training,twitter.training)
trainingcorpus <- VCorpus(VectorSource(trainingset))

# @param document   a corpora object
preprocess <- function(document){
    # tm_map runs a function over a corpora object
    document <- tm_map(document, removePunctuation)
    document <- tm_map(document, removeNumbers)
    document <- tm_map(document, stripWhitespace)
    document <- tm_map(document, content_transformer(tolower))
    document <- tm_map(document, PlainTextDocument)
    return(document)
}

trainingcorpus <- preprocess(trainingcorpus)

# Create tokenizers i.e. functions that will split the words into appropriate lengths
Unigramtokenizer <- function(x)
        unlist(lapply(ngrams(words(x), 1), paste, collapse = " "), use.names = FALSE)
Bigramtokenizer <- function(x)
        unlist(lapply(ngrams(words(x), 2), paste, collapse = " "), use.names = FALSE)
Trigramtokenizer <-function(x)
        unlist(lapply(ngrams(words(x), 3), paste, collapse = " "), use.names = FALSE)

unigramdocumentmatrix <- TermDocumentMatrix(trainingcorpus,control = list(tokenize = Unigramtokenizer))
bigramdocumentmatrix <- TermDocumentMatrix(trainingcorpus,control = list(tokenize = Bigramtokenizer))
trigramdocumentmatrix <- TermDocumentMatrix(trainingcorpus,control = list(tokenize = Trigramtokenizer))

# compute frequences of the various ngrams
# will drop ngrams with frequencies less than 50 occurrences
unigramf <- findFreqTerms(unigramdocumentmatrix,lowfreq =50)
bigramf <- findFreqTerms(bigramdocumentmatrix,lowfreq = 50)
trigramf <- findFreqTerms(trigramdocumentmatrix,lowfreq = 50)

Unigramfreq <- rowSums(as.matrix(unigramdocumentmatrix[unigramf,]))
Unigramfreq <- data.frame(word=names(Unigramfreq),frequency=Unigramfreq)
print(head(Unigramfreq))
Bigramfreq <- rowSums(as.matrix(bigramdocumentmatrix[bigramf,]))
Bigramfreq <- data.frame(word=names(Bigramfreq),frequency=Bigramfreq)
print(head(Bigramfreq))
Trigramfreq <- rowSums(as.matrix(trigramdocumentmatrix[trigramf,]))
Trigramfreq <- data.frame(word=names(Trigramfreq),frequency=Trigramfreq)
print(head(Trigramfreq))


plotthegraph <- function(data,title,num){
    df <- data[order(-data$frequency),][1:num,]
    barplot(df[1:num,]$freq, las = 2, names.arg = df[1:num,]$word,
            col ="red", main = title,
            ylab = "Word frequencies",cex.axis =0.8)
}
par(mfcol=c(3,1))
par(mar=c(10,4,4,2))
plotthegraph(Unigramfreq,"Top Unigrams",20)
plotthegraph(Bigramfreq,"Top Bigrams",20)
plotthegraph(Trigramfreq,"Top Trigrams",20)
par(mfcol=c(1,1))



