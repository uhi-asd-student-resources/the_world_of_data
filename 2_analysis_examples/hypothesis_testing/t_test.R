# T-test examples
#
# Reference: http://www.sthda.com/english/wiki/one-sample-t-test-in-r
#

library("ggpubr")

set.seed(1234)
my_data <- data.frame(
  name = paste0(rep("M_", 10), 1:10),
  weight = round(rnorm(10, 20, 2), 1)
)

# Print the first 10 rows of the data
head(my_data, 10)

# Statistical summaries of weight
summary(my_data$weight)

ggboxplot(my_data$weight, 
          ylab = "Weight (g)", xlab = FALSE,
          ggtheme = theme_minimal())

# Use shapiro.test to test for normality
# H(0) : data is normally distributed
# H(1) : data is not normally distributed
p <- shapiro.test(my_data$weight) # => p-value = 0.6993


# draw a qqplot that compares the distribution of the data 
# against the idealised normal distribution
# if its approximately a straight line then we are all good.
ggqqplot(my_data$weight, ylab = "Men's weight",
         ggtheme = theme_minimal())

# if this was not normally distributed we could use the one-sample 
# wilcoxon rank test, which is a non-parametric test.

# Compute the One-sample t-test
res <- t.test(my_data$weight, mu = 25)
# Printing the results
print(res )