# Analysis Methods

## Installation

Requires:
* libcurl-devel
* openssl-devel
* libgit2-devel

To install the required dependencies run the following within R:

```
source("packages.R")
```


## Methods of interest

1. Experimental design. He/She should be able to set up an experiment and draw valid
conclusions out of it, for example choosing test and trial groups, type of experiment,
controlled independent variable, randomization, replication, etc.
2. Expectation maximization algorithms 
   1. K-Means
   2. Gaussian mixture 
   3. density estimation
3. . Similarity of preference:
   1. Jaccard Index
4. Document selection
   1. n-gram model, and vector space model
5. Hypothesis testing for a variety of distributions like 
   1. t-tests, 
   2. chi-square, 
   3. ANOVA
   4. be able to calculate the minimum sample size for statistical significance
6. Statistical tests like the classic 
   1. Kolmogorov-Smirnoff or Shapiro-Wilks for testing normality of data, 
   2. Dickey-Fuller or Phillips-Perron test for stationarity, 
   3. and Breusch-Pagan test for heteroskedasticity.
7. Classification algorithms like 
   1. Support Vector Machines (SVM), 
   2. Random Forest, 
   3. Gradient Boosting, 
   4. K-Nearest Neighbour (KNN) 
   5. Multinomial Naïve-Bayes Classifier
   4. Gaussian Naïve-Bayes Classifier
8. Multivariate analysis: 
   1. covariance and correlation matrices, 
   2. Multivariate regressions including 
      1. linear regression
      2. logistic regression, 
      3. exponential, 
      4. logarithmic, 
      5. and polynomial.
9. Time series including 
   1.  ARIMA - trend and seasonality, 
   2.  ARCH  - volatility of a process limited to linear functions, 
   3.  GARCH - volatility fo a process includes lagged conditional variance,
   4.  EWMA - exponential weighted moving average, 
   5.  Holt-Winters seasonal method,
   6.  and Fourier analysis
   7.  Online references: https://otexts.com/fpp2/
10. Dimensionality reduction methods like 
    1.  Principle Components Analysis (PCA), 
    2.  Common Factor Analysis (CFA),
    3.  Image Factoring
    4.  Maximum likelihood
    5.  t-distributed stochastic neighbour embedding (TSNE) 
    6.  uniform manifold approximation and projection (UMAP)
11. Optimization algorithms like 
    1.  linear programming, 
    2.  dynamic programming, 
    3.  Minimax or Maximin, 
    4.  stochastic optimization and 
    5.  Monte Carlo Tree Search
12. Graph optimization like 
    1.  PERT, 
    2.  Dijkstra's Hungarian model,
    3.  Ford-Fulkerson,
    4.  Markov chains modelling.
13. Good mathematical knowledge of Taylor series to approximate functions, advanced calculus, Lagrange polynomials or parametric interpolation
14. Portfolio Optimisation
    1.  Mean Variance Portfolio Optimisation

